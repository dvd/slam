#include <iostream>
#include <string>
#include <vector>
namespace slam {
    using std::string;
    using std::vector;

    namespace io {
        using std::ostream;

        enum class TagType { standard, self_close };
        enum class NodeStatus { open, close };

        class Renderer{
            private:
                int current_level;
            public:

                class Node {
                        Renderer *renderer;
                        string const& tag_name;
                        NodeStatus status;
                    public:
                        Node(Renderer*, string const&, NodeStatus);
                        void close();
                };

                int indent;
                ostream& stream;
                Renderer(ostream&);

                Renderer::Node addTag(string const&, TagType const);
                void closeTag(string const&);
        };

        Renderer::Renderer(ostream& s) : current_level(0), indent(4), stream(s) {}

        Renderer::Node Renderer::addTag(string const& tag_name, TagType const tag_type=TagType::standard) {
            auto padding = string(this->indent * this->current_level, ' ');
            auto current_status = NodeStatus::open;

            if(tag_type == TagType::self_close) {
                this->stream << padding << "<" << tag_name << " />\n";
                current_status = NodeStatus::close;
            }
            else {
                this->stream << padding << "<" << tag_name << ">\n";
                this->current_level++;
            }

            return Renderer::Node(this, tag_name, current_status);
        }

        void Renderer::closeTag(string const& tag_name) {
            this->current_level--;

            auto padding = string(this->indent * this->current_level, ' ');
            this->stream << padding << "</" << tag_name << ">\n";
        }

        Renderer::Node::Node(Renderer* r, string const& name, NodeStatus node_status) : renderer(r), tag_name(name), status(node_status) {}

        void Renderer::Node::close() {
            if(this->status == NodeStatus::open) {
                this->renderer->closeTag(this->tag_name);
                this->status = NodeStatus::close;
            }
        }
    };

    struct Token {
        vector<Token*> children;
        virtual void resolve(slam::io::Renderer&) = 0;
    };

    struct Tag : Token {
        string name;
        slam::io::TagType tag_type;

        Tag(string const&);

        void resolve(slam::io::Renderer&);
    };

    Tag::Tag(string const& n) : name(n) {
        if(n == "img") {
            this->tag_type = slam::io::TagType::self_close;
        }
        else {
            this->tag_type = slam::io::TagType::standard;
        }
    }

    void Tag::resolve(slam::io::Renderer& r) {
        auto node = r.addTag(this->name, this->tag_type);
        for(auto c : this->children) {
            c->resolve(r);
        }
        node.close();
    }

    struct Conditional : Token {
    };

    namespace ctx {
        struct Context {};
    };

};

int main() {
    using namespace slam;
    using namespace slam::io;

    Tag div("div");
    Tag span("span");
    Tag img("img");
    Tag span2("span");

    div.children.push_back(&span);
    span.children.push_back(&img);
    div.children.push_back(&span2);

    auto r = Renderer(std::cout);

    div.resolve(r);
    return 0;
}
