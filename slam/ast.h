#pragma once
#include <map>
#include <string>
#include <vector>
#include "slam/io.h"

namespace slam { namespace ast {

using std::string;
using std::vector;

struct Node {
    vector<Node*> children;

    virtual ~Node();
    virtual void apply(slam::io::Renderer*) const = 0;
};

struct Tag : Node {
    enum class TagType { standard, self_close };

    string name;
    TagType type{TagType::standard};
    std::map<string, string> attribs;

    Tag();
    Tag(string const&);
    Tag(string const&, std::map<string, string> const&);

    void apply(slam::io::Renderer*) const;
};

}}
