#include <string>
#include <vector>
#include "slam/ast.h"

namespace slam { namespace ast {

Node::~Node() {}

Tag::Tag() : Tag{""} {}
Tag::Tag(string const& name) : Tag{name, {}} {}
Tag::Tag(string const& name, std::map<string, string> const& attribs) : name(name), attribs(attribs) {}

void Tag::apply(slam::io::Renderer* r) const {
    if(this->type == Tag::TagType::standard || this->children.size() > 0) {
        r->openTag(this->name, this->attribs);
        for(auto& child : this->children) {
            child->apply(r);
        }
        r->closeTag(this->name);
    }
    else {
        r->putTag(this->name);
    }
}

}}
