#pragma once
#define BOOST_RESULT_OF_USE_DECLTYPE
#define BOOST_SPIRIT_USE_PHOENIX_V3
#include <map>
#include <string>
#include <vector>
#include <boost/fusion/include/std_pair.hpp>
#include <boost/spirit/include/qi.hpp>
#include <boost/spirit/include/phoenix.hpp>
#include <boost/spirit/include/phoenix_fusion.hpp>
#include <boost/spirit/include/phoenix_stl.hpp>
#include <boost/spirit/include/phoenix_operator.hpp>

#include "slam/ast.h"

namespace slam { namespace parser {

namespace qi = boost::spirit::qi;
namespace phx = boost::phoenix;
namespace fusion = boost::fusion;

using slam::ast::Node;
using slam::ast::Tag;
using std::string;

template<typename Iterator>
struct XSlamGrammar : qi::grammar<Iterator, Node*()> {
    qi::rule<Iterator, Node*()> expression;

    qi::rule<Iterator, Node*(int), qi::locals<int>> block;

    qi::rule<Iterator, Node*()> node;
    qi::rule<Iterator, Tag*()> tag;
    qi::rule<Iterator, int(int)> indent;

    qi::rule<Iterator, string()> text_content;

    qi::rule<Iterator, std::map<string, string>(), qi::locals<char>> attributes_list;
    qi::rule<Iterator, std::pair<string, string>()> attribute;
    qi::rule<Iterator, string()> attribute_name;
    qi::rule<Iterator, string()> attribute_value;

    qi::rule<Iterator, string(), qi::locals<char>> quoted_string;
    qi::rule<Iterator, string()> identifier;
    qi::symbols<char const, char const> escape_chars;
    qi::symbols<char const, char> delimiters;

    XSlamGrammar() : XSlamGrammar::base_type(expression) {

        // html         0 Node1
        //   head       2 Node1->Node2
        //     meta     4 Node1->Node2->Node3
        //   body       2 Node1->Node4

        //   head       2 Node1->Node2
        //     meta     4 Node1->Node2->Node3
        //   body       2 Node1->Node4
        using qi::_1;
        using qi::_a;
        using qi::_r1;
        using qi::_val;
        using phx::bind;
        using phx::push_back;

        expression %= block(0);

        block =
            indent(_r1)[_a = _1]
            >> node[_val = _1]
            >> *block(_a + 1)[push_back(phx::val(_val)->*&Node::children, _1)];

        node %= (tag >> (qi::eol | qi::eoi));

        {
            using AttrType = boost::optional<std::map<string, string>>;
            using TextType = boost::optional<string>;
            tag = (identifier >> -attributes_list >> -text_content) [
                _val = bind([](string const& name, AttrType const& attrs, TextType const& text) -> Tag* {
                    return new Tag{name, attrs ? *attrs : std::map<string, string>() };
                }, _1, qi::_2, qi::_3)
            ];
        }

        // matches at least _r1 spaces -> expose the number of matched spaces
        indent = (qi::repeat(_r1, qi::inf)[qi::blank])[_val = phx::size(_1)];

        text_content = *(qi::char_ - qi::eol);

        attributes_list = qi::skip(qi::blank)[
            (
                qi::omit[delimiters[_a = _1]]
                >> *attribute
                > qi::omit[qi::char_(_a)]
            )
            | *attribute
        ];
        attribute = qi::skip(qi::space)[attribute_name >> "=" >> attribute_value];
        attribute_name = identifier.alias();
        attribute_value = quoted_string.alias();

        quoted_string =
            qi::omit[qi::char_("'\"")[_a = _1]]
            >> qi::lexeme[(*(escape_chars | (qi::char_ - qi::char_(_a)) ))]
            >> qi::lit(_a);

        identifier = +qi::alpha >> *qi::alnum;

        escape_chars.add
            ("\\a", '\a')
            ("\\b", '\b')
            ("\\f", '\f')
            ("\\n", '\n')
            ("\\r", '\r')
            ("\\t", '\t')
            ("\\v", '\v')
            ("\\\\", '\\')
            ("\\\'", '\'')
            ("\\\"", '\"');

        delimiters.add
            ("(", ')')
            ("[", ']')
            ("{", '}');

        this->expression.name("expression");
        this->block.name("block");
        this->node.name("node");
        this->tag.name("tag");
        this->indent.name("indent");

        this->text_content.name("text");

        this->attributes_list.name("attributes_list");
        this->attribute.name("attribute");
        this->attribute_name.name("attribute_name");
        this->attribute_value.name("attribute_value");

        this->quoted_string.name("quoted_string");
        this->identifier.name("identifier");

        //qi::debug(this->expression);
        //qi::debug(this->block);
        //qi::debug(this->node);
        //qi::debug(this->tag);
        //qi::debug(this->indent);

        //qi::debug(this->text_content);

        //qi::debug(this->attributes_list);
        //qi::debug(this->attribute);
        //qi::debug(this->attribute_name);
        //qi::debug(this->attribute_value);

        //qi::debug(this->quoted_string);
        //qi::debug(this->identifier);
    }
};

Node* parse(string const& src) {
    XSlamGrammar<string::const_iterator> g;
    auto start = src.begin();
    auto end = src.end();
    Node* result = nullptr;
    qi::parse(start, end, g, result);
    return result;
}

}}
