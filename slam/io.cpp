#include <iostream>
#include "slam/ast.h"
#include "slam/io.h"

namespace slam { namespace io {

Renderer::Renderer(ostream& stream)
    : Renderer(stream, Settings()) {}
Renderer::Renderer(ostream& stream, Settings settings)
    : stream(stream), settings(settings) {}

inline string Renderer::padding() const {
    return string(this->settings.indent * this->level, ' ');
}

void Renderer::apply(ast::Node const* root) {
    root->apply(this);
}

void Renderer::putTag(string const& tag_name) {
    this->stream << this->padding() << "<" << tag_name << " />" << std::endl;
}

void Renderer::openTag(string const& tag_name) {
    this->stream << this->padding() << "<" << tag_name << ">" << std::endl;
    this->level += 1;
}

void Renderer::openTag(string const& tag_name, std::map<string, string> const& attribs) {
    this->stream
        << this->padding()
        << "<" << tag_name;
    if(attribs.size()) {
        this->stream << " ";
        for(auto& x : attribs) {
            this->stream << x.first << "=\"" << x.second << "\" ";
        }
    }
    this->stream << ">" << std::endl;
    this->level += 1;
}

void Renderer::closeTag(string const& tag_name) {
    this->level -= 1;
    this->stream << this->padding() << "</" << tag_name << ">" << std::endl;
}

render::render(ast::Node const* root)
    : render(root, Settings()) {}
render::render(ast::Node const* root, Settings settings)
    : root(root), settings(settings) {}

ostream& operator<<(ostream& stream, render const& r) {
    Renderer(stream, r.settings).apply(r.root);
    return stream;
}

}}
