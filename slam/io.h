#pragma once
#include <iostream>
#include <map>
#include <string>

namespace slam {
namespace ast {
    struct Node;
}
namespace io {

using std::string;
using std::ostream;

enum class NodeStatus { open, close };

struct Settings {
    int indent = 4;
};

struct Renderer {
    private:
        int level = 0;
        ostream& stream;
        Settings const settings;
    protected:
        string padding() const;
    public:
        Renderer(ostream&);
        Renderer(ostream&, Settings);

        void apply(ast::Node const*);
        void putTag(string const&);
        void openTag(string const&);
        void openTag(string const&, std::map<string, string> const&);
        void closeTag(string const&);
};

struct render {
    ast::Node const* root;
    Settings const settings;
    render(ast::Node const*);
    render(ast::Node const*, Settings);
};

ostream& operator<<(ostream&, render const&);
}}
