#include <iostream>
#include <string>
#include "slam.h"

using namespace std;

int main() {
    using namespace slam::ast;
    auto const src = string("html\n  head\n    meta\n  body\n    h1\n");
    //auto const src = string("html\n  head\n");
    cout << "input: " << src << endl;
    auto a = slam::parser::parse(src);

    cout << "src " << a << endl;

    cout << slam::io::render(a);
    return 0;
}
