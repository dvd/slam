#pragma once

#include <iostream>
#include <iterator>
#include <sstream>
#include <string>
#include <vector>
#include <stdexcept>

namespace {
    using std::runtime_error;
    using std::ostringstream;

    void _supplant(ostringstream& buffer, char const* msg) {
        while(*msg) {
            if(*msg == '%') {
                throw runtime_error("invalid message: too many placeholders");
            }
            buffer << *msg++;
        }
    }

    template<typename T, typename... Args>
    void _supplant(ostringstream& buffer, char const* msg, T arg0, Args... args) {
        while(*msg) {
            if(*msg == '%') {
                buffer << arg0;
                return _supplant(buffer, msg+1, args...);
            }
            buffer << *msg++;
        }
        throw runtime_error("invalid message: too many parameters");
    }
};

template<typename... Args>
std::string supplant(char const* msg, Args... args) {
    std::ostringstream buffer;
    _supplant(buffer, msg, args...);
    return buffer.str();
}

// wizardy from http://stackoverflow.com/questions/9404189/detecting-the-parameter-types-in-a-spirit-semantic-action
#include <cxxabi.h>
#include <stdlib.h>
#include <string>
#include <iostream>

template <typename T> std::string nameofType(const T& v) {
    int status;
    char *realname = abi::__cxa_demangle(typeid(v).name(), 0, 0, &status);
    std::string name(realname ? realname : "????");
    free(realname);
    return name;
}

struct what_is_the_attr {
    template <typename> struct result { typedef bool type; };

    template <typename T> bool operator()(T& attr) const {
        std::cerr << "what_is_the_attr: " << nameofType(attr) << std::endl;
        return true;
    }
};

struct what_are_the_arguments {
    template <typename...> struct result { typedef bool type; };

    template <typename... T> bool operator()(const T&... attr) const {
        std::vector<std::string> names { nameofType(attr)... };
        std::cerr << "what_are_the_arguments:\n\t";
        std::copy(names.begin(), names.end(), std::ostream_iterator<std::string>(std::cerr, "\n\t"));
        std::cerr << '\n';
        return true;
    }
};
