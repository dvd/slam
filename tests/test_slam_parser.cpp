#include <map>
#include <tuple>
#include "slam.h"
#include "gtest/gtest.h"
#include <boost/spirit/include/qi.hpp>

namespace qi = boost::spirit::qi;

using std::string;
using std::tuple;
using std::get;
using std::make_tuple;
using slam::ast::Tag;
using slam::parser::parse;
using slam::parser::XSlamGrammar;

struct SingleGrammarRuleTest : ::testing::Test {
    XSlamGrammar<string::const_iterator> grammar;

    template<typename T>
    tuple<bool, typename T::attr_type> parse(string const& input, T rule) {
        typename T::attr_type result;
        bool parsed = false;
        try {
            parsed = qi::parse(input.begin(), input.end(), rule, result);
        } catch(qi::expectation_failure<string::const_iterator>& e) {
            result = typename T::attr_type{};
        }
        return make_tuple(parsed, result);
    }

    template<typename T, typename R>
    void run_test(T const& tests_list, R const& rule) {
        for(auto& test : tests_list) {
            auto result = this->parse(get<0>(test), rule);
            auto expected = get<1>(test);
            auto success = get<0>(result);
            EXPECT_EQ(expected, success) << " input --> " << get<0>(test);
            if(expected && success) {
                EXPECT_EQ(get<2>(test), get<1>(result));
            }
        }
    }
};

struct QuotedStringRuleTest : SingleGrammarRuleTest {
    using RType = string;
    std::vector<tuple<string, bool, RType>> tests_list = {
        make_tuple(R"("hello world")", true, RType{"hello world"}),
        make_tuple(R"('hello world')", true, RType{"hello world"}),
        make_tuple(R"("hello \"world\"")", true, RType{"hello \"world\""}),
        make_tuple(R"('hello \'world\'')", true, RType{"hello \'world\'"}),
        make_tuple(R"("hello 'world'")", true, RType{"hello 'world'"}),
        make_tuple(R"('hello "world"')", true, RType{"hello \"world\""}),
        make_tuple(R"("hello\tworld")", true, RType{"hello\tworld"}),
        make_tuple(R"("hello world)", false, RType{}),
        make_tuple(R"('hello world)", false, RType{})
    };
};
TEST_F(QuotedStringRuleTest, test) {
    run_test(tests_list, grammar.quoted_string);
}

struct IdentifierRuleTest : SingleGrammarRuleTest {
    using RType = string;
    std::vector<tuple<string, bool, RType>> tests_list = {
        make_tuple("html", true, RType{"html"}),
        make_tuple("h1", true, RType{"h1"}),
        make_tuple("1h", false, RType{}),
        make_tuple("(h", false, RType{})
    };
};
TEST_F(IdentifierRuleTest, test) {
    run_test(tests_list, grammar.identifier);
}

struct AttributeRuleTest : SingleGrammarRuleTest {
    using RType = std::pair<string, string>;
    std::vector<tuple<string, bool, RType>> tests_list = {
        make_tuple(
            "class=\"title selected\"", true,
            RType{"class", "title selected"}),

        make_tuple(
            "class = \"title selected\"", true,
            RType{"class", "title selected"})
    };
};
TEST_F(AttributeRuleTest, test) {
    run_test(tests_list, grammar.attribute);
}

struct AttributesListRuleTest : SingleGrammarRuleTest {
    using RType = std::map<string, string>;
    std::vector<tuple<string, bool, RType>> tests_list = {
        make_tuple("", true, RType{}),
        make_tuple("()", true, RType{}),
        make_tuple("[]", true, RType{}),
        make_tuple("{}", true, RType{}),
        make_tuple(
            R"(class="title")", true,
            RType{{"class", "title"}}),
        make_tuple(
            R"(id="page" class="title")", true,
            RType{{"id", "page"}, {"class", "title"}}),
        make_tuple(
            R"(class='title')", true,
            RType{{"class", "title"}}),
        make_tuple(
            R"(class="title \"quoted\"")", true,
            RType{{"class", "title \"quoted\""}}),
        make_tuple(
            R"((class="title"))", true,
            RType{{"class", "title"}}),
        make_tuple(
            R"([class="title"])", true,
            RType{{"class", "title"}}),
        make_tuple(
            R"({class="title"})", true,
            RType{{"class", "title"}}),
        make_tuple(
            R"(( class = "title" ))", true,
            RType{{"class", "title"}}),
        make_tuple(
            R"((class="title")", false,
            RType{}),
        make_tuple(
            R"([class="title")", false,
            RType{}),
        make_tuple(
            R"({class="title")", false,
            RType{})
    };
};
TEST_F(AttributesListRuleTest, test) {
    run_test(tests_list, grammar.attributes_list);
}

struct TextContentRuleTest : SingleGrammarRuleTest {
    using RType = string;
    std::vector<tuple<string, bool, RType>> tests_list = {
        make_tuple("", true, RType{}),
        make_tuple("\n", true, RType{}),
        make_tuple("\r\n", true, RType{}),
        make_tuple("\r", true, RType{}),
        make_tuple("hello world", true, RType{"hello world"}),
        make_tuple(R"(hello "world")", true, RType{"hello \"world\""}),
        make_tuple("hello\n", true, RType{"hello"}),
        make_tuple("hello\r\n", true, RType{"hello"}),
        make_tuple("hello\r", true, RType{"hello"})
    };
};
TEST_F(TextContentRuleTest, test) {
    run_test(tests_list, grammar.text_content);
}

TEST(Parsing, just_one_tag) {
    auto result = static_cast<Tag*>(parse("html"));
    ASSERT_NE(result, nullptr);

    EXPECT_EQ(result->name, "html");
    EXPECT_EQ(result->children.size(), 0);
    EXPECT_EQ(result->attribs.size(), 0);
}

TEST(Parsing, one_child) {
    auto result = static_cast<Tag*>(parse("html\n  head"));
    ASSERT_NE(result, nullptr);

    EXPECT_EQ(result->name, "html");
    EXPECT_EQ(result->children.size(), 1);

    auto child = static_cast<Tag*>(result->children[0]);
    EXPECT_EQ(child->name, "head");
}

TEST(Parsing, one_tag_one_attrib) {
    auto result = static_cast<Tag*>(parse("html id=\"42\""));
    ASSERT_NE(result, nullptr);

    EXPECT_EQ(result->name, "html");
    EXPECT_EQ(result->children.size(), 0);
    EXPECT_EQ(result->attribs.size(), 1);
    EXPECT_EQ(result->attribs["id"], "42");
}
