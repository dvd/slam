#include <functional>
#include <string>
#include <tuple>
#include <vector>
#include "expr.h"
#include "gtest/gtest.h"
#include "tests/utils.h"

using std::make_tuple;
using std::string;
using std::tuple;
using std::vector;

using expr::ast::Node;
using expr::parser::ExprGrammar;

using expr::types::expr_type;
using SingleRule = slam::test::SingleGrammarRuleTest<ExprGrammar<string::const_iterator>, expr_type>;

struct InterpolationRuleTest : SingleRule {
    void SetUp() {
        SingleRule::SetUp();
        this->context->put("a", 42);
        this->context->put("b", "hello");
    }
};
TEST_P(InterpolationRuleTest, test) {
    test_rule(grammar.interpolation, this->GetParam());
}
INSTANTIATE_TEST_CASE_P(
    Instance1,
    InterpolationRuleTest,
    ::testing::Values(
        make_tuple("#{1}", true, "1"),
        make_tuple("#{1+2}", true, "3"),
        make_tuple("#{ 2 + 2 }", true, "4"),
        make_tuple("#{a*2}", true, "84"),
        make_tuple("#{a+a}", true, "84"),
        make_tuple("#{a+a/2}", true, "63"),
        make_tuple("#{(a+a)/0.5}", true, "168"),
        make_tuple("#{b}", true, "hello"),
        make_tuple("#{b+b}", true, "hellohello"),
        make_tuple("#{", false, "0"),
        make_tuple("#{}", false, "0"),
        make_tuple("{}", false, "0")
));

struct StringRuleTest : SingleRule {
    void SetUp() {
        SingleRule::SetUp();
        using expr::types::wrap_function;
        this->context->put("a", 42);
        this->context->put("b", "hello");
        this->context->put("c", "hello world");
        this->context->put("vd", vector<double>{1,2,3});
        this->context->put("vs", vector<string>{"1", "2", "3"});

        std::function<double(double, double)> mul = std::bind(
            &StringRuleTest::mul, this, std::placeholders::_1, std::placeholders::_2);
        this->context->put("mul", wrap_function(mul));

    }
    double mul(double a, double b) {
        return a*b;
    }
};
TEST_P(StringRuleTest, test) {
    test_rule(grammar.string_value, this->GetParam());
}
INSTANTIATE_TEST_CASE_P(
    Instance1,
    StringRuleTest,
    ::testing::Values(
        make_tuple("\"abc\"", true, "abc"),
        make_tuple("'abc'", true, "abc"),
        make_tuple("'hello world'", true, "hello world"),
        make_tuple(R"('hello \"world\"')", true, "hello \"world\""),
        make_tuple("'#{3}'", true, "3"),
        make_tuple("'#{3+3}'", true, "6"),
        make_tuple("'#{ 3 + 3 }'", true, "6"),
        make_tuple("'#{a}'", true, "42"),
        make_tuple("'hello #{a}'", true, "hello 42"),
        make_tuple("'#{a+a}'", true, "84"),
        make_tuple("'#{b}'", true, "hello"),
        make_tuple("'#{b+b}'", true, "hellohello"),
        make_tuple("'#{c}'", true, "hello world"),
        make_tuple("'#{b} #{a}'", true, "hello 42"),
        make_tuple("'#{b}#{b}'", true, "hellohello"),
        make_tuple("' #{a} '", true, " 42 "),
        make_tuple("'#{vd}'", true, "{vector of numbers size=3}"),
        make_tuple("'#{vs}'", true, "{vector of strings size=3}"),
        make_tuple("'#{mul}'", true, "{callable}"),
        make_tuple("'#{mul(21, 2)}'", true, "42"),
        make_tuple("'#{mul(a, 2)}'", true, "84"),
        make_tuple("'#{mul(a, a)}'", true, "1764")
));

using expr::parser::parse;
using expr::context::Context;

double mul(double a, double b) {
    return a*b;
}

class SimpleExprParserTest : public ::testing::Test {
    public:
        Context ctx;

        virtual void SetUp() {
            using expr::types::wrap_function;
            using namespace std::placeholders;

            ctx.put("pi", 3.1415);
            ctx.put("greet", "hello world");
            ctx.put("mul", wrap_function(mul));
            {
                std::function<double(double)> f = std::bind(&SimpleExprParserTest::doubler, this, _1);
                ctx.put("doubler", wrap_function(f));
            }
            {
                std::function<double()> f = std::bind(&SimpleExprParserTest::fpi, this);
                ctx.put("fpi", wrap_function(f));
            }
            {
                std::function<double(double,double)> f = std::bind(&SimpleExprParserTest::add<double>, this, _1, _2);
                ctx.put("sum", wrap_function(f));
            }
            {
                std::function<string(string,string)> f = std::bind(&SimpleExprParserTest::add<string>, this, _1, _2);
                ctx.put("concat", wrap_function(f));
            }
        }

        double fpi() {
            return 3.1415;
        }

        double doubler(double a) {
            return a*2;
        }

        template<typename T>
        T add(T const& a, T const& b) {
            return a + b;
        }
};

TEST_F(SimpleExprParserTest, Number) {
    auto node = parse("3");
    auto result = node->evaluate(ctx);
    EXPECT_EQ(boost::get<double>(result), 3);
}

TEST_F(SimpleExprParserTest, String) {
    auto node1 = parse("'hello world'");
    auto result1 = node1->evaluate(ctx);
    EXPECT_EQ(boost::get<string>(result1), "hello world");

    auto node2 = parse("\"hello world\"");
    auto result2 = node2->evaluate(ctx);
    EXPECT_EQ(boost::get<string>(result2), "hello world");
}

TEST_F(SimpleExprParserTest, NumberSymbol) {
    auto node = parse("pi");
    auto result = node->evaluate(ctx);
    EXPECT_EQ(boost::get<double>(result), 3.1415);
}

TEST_F(SimpleExprParserTest, StringSymbol) {
    auto node = parse("greet");
    auto result = node->evaluate(ctx);
    EXPECT_EQ(boost::get<string>(result), "hello world");
}

TEST_F(SimpleExprParserTest, Function0Args) {
    auto node = parse("fpi()");
    auto result = node->evaluate(ctx);
    EXPECT_EQ(boost::get<double>(result), 3.1415);
}

TEST_F(SimpleExprParserTest, PlainFunction) {
    auto node = parse("mul(3, 2)");
    auto result = node->evaluate(ctx);
    EXPECT_EQ(boost::get<double>(result), 6);
}

TEST_F(SimpleExprParserTest, MemberFunction) {
    auto node = parse("doubler(1)");
    auto result = node->evaluate(ctx);
    EXPECT_EQ(boost::get<double>(result), 2);
}

TEST_F(SimpleExprParserTest, SumFunctionFromTemplate) {
    auto node = parse("sum(1,2)");
    auto result = node->evaluate(ctx);
    EXPECT_EQ(boost::get<double>(result), 3);
}

TEST_F(SimpleExprParserTest, ConcatFunctionFromTemplate) {
    auto node = parse("concat('hello', 'world')");
    auto result = node->evaluate(ctx);
    EXPECT_EQ(boost::get<string>(result), "helloworld");
}

TEST_F(SimpleExprParserTest, OperatorPrecedence) {
    auto node = parse("3+6*8-27/3");
    auto result = node->evaluate(ctx);
    EXPECT_EQ(boost::get<double>(result), 42);
}

TEST_F(SimpleExprParserTest, GroupPrecedence) {
    auto node = parse("(2*3*(4+1)-2)/(2*(2+4*3))");
    auto result = node->evaluate(ctx);
    EXPECT_EQ(boost::get<double>(result), 1);
}

TEST_F(SimpleExprParserTest, FunctionCallWithSymbols) {
    auto node = parse("doubler(pi)");
    auto result = node->evaluate(ctx);
    EXPECT_FLOAT_EQ(boost::get<double>(result), 6.283);
}

TEST_F(SimpleExprParserTest, NestedFunctionCall) {
    auto node = parse("3*((mul(fpi(), 4)+3)*(2+pi))");
    auto result = node->evaluate(ctx);
    EXPECT_FLOAT_EQ(boost::get<double>(result), 240.09776);
}
