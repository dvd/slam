#pragma once
#include <string>
#include <tuple>
#include "gtest/gtest.h"
#include <boost/spirit/include/qi.hpp>
#include "expr.h"

namespace slam { namespace test {

using std::get;
using std::string;
using std::tuple;
using std::make_tuple;

template<typename TGrammar, typename TResult>
struct SingleGrammarRuleTest : ::testing::TestWithParam<tuple<char const*, bool, TResult>> {
    using PType = tuple<char const*, bool, TResult>;
    TGrammar grammar;
    expr::context::Context* context;

    struct are_strict_equals : boost::static_visitor<bool>
    {
        template<typename T>
        bool operator()( T const& lhs, T const& rhs ) const {
            EXPECT_EQ(lhs, rhs);
            return lhs == rhs;
        }

        template<typename T, typename U>
        bool operator()(T const&, U const&) const {
            ADD_FAILURE() << "cannot compare different types";
            return false;
        }

        template<typename T>
        bool operator()(T const&, expr::types::callable const&) const {
            ADD_FAILURE() << "callable cannot be compared";
            return false;
        }

        template<typename T>
        bool operator()(expr::types::callable const&, T const&) const {
            ADD_FAILURE() << "callable cannot be compared";
            return false;
        }

        bool operator()(expr::types::callable const&, expr::types::callable const&) const {
            ADD_FAILURE() << "callable cannot be compared";
            return false;
        }
    };

    virtual void SetUp() {
        context = new expr::context::Context();
    }
    virtual void TearDown() {
        delete context;
        context = nullptr;
    }

    template<typename T>
    tuple<bool, typename T::attr_type> parse(string const& input, T rule) {
        typename T::attr_type result;
        bool parsed = false;

        namespace qi = boost::spirit::qi;
        try {
            parsed = qi::phrase_parse(input.begin(), input.end(), rule, qi::space, result);
        } catch(qi::expectation_failure<string::const_iterator>& e) {
            result = typename T::attr_type{};
        }
        return make_tuple(parsed, result);
    }

    template<typename R>
    void test_rule(R const& rule, PType const& test) {
        auto parsing_result = this->parse(get<0>(test), rule);
        auto success = get<0>(parsing_result);

        EXPECT_EQ(get<1>(test), success) << " input --> " << get<0>(test);
        if(get<1>(test) && success) {
            auto node = get<1>(parsing_result);

            auto expected_value = get<2>(test);
            auto value = node->evaluate(*this->context);

            boost::apply_visitor(are_strict_equals(), expected_value, value);
        }
    }
};

}};
