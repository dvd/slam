#pragma once

#include <initializer_list>
#include <map>
#include <string>
#include <vector>
#include <iostream>
#include "expr/types.h"

namespace expr {
    namespace context {
        using std::map;
        using std::ostream;
        using std::string;
        using expr::types::expr_type;

        class Context {
            map<string, expr_type> store;

            friend void dump(ostream&, Context const&);

            public:
                using value_type = map<string, expr_type>::value_type;

                Context();
                Context(std::initializer_list<value_type>);

                expr_type& put(string const&, expr_type const&);
                expr_type  get(string const&) const;
        };

        Context::Context() {}
        Context::Context(std::initializer_list<Context::value_type> l) : store(l) {}

        expr_type& Context::put(string const& key, expr_type const& value) {
            return this->store[key] = value;
        }

        expr_type Context::get(string const& key) const {
            return this->store.at(key);
        }

        void dump(ostream& out, Context const& ctx) {
            using expr::types::callable;
            using std::endl;

            out << "-- context dump" << endl;
            for(auto e : ctx.store) {
                auto key = std::get<0>(e);
                auto value = std::get<1>(e);

                out << "* " << key << " -> ";

                if(auto p_double = boost::get<double>(&value)) {
                    out << *p_double;
                }
                else if(auto p_string = boost::get<string>(&value)) {
                    out << *p_string;
                }
                else if(auto p_vec_double = boost::get<std::vector<double>>(&value)) {
                    out << "[ ";
                    for(auto const& e : *p_vec_double) {
                        out << e << " ";
                    }
                    out << "]";
                }
                else if(auto p_vec_double = boost::get<std::vector<string>>(&value)) {
                    out << "[ ";
                    for(auto const& e : *p_vec_double) {
                        out << "\"" << e << "\" ";
                    }
                    out << "]";
                }
                else if(auto p_callable = boost::get<callable>(&value)) {
                    out << "callable[" << p_callable << "]";
                }
                else {
                    out << "?";
                }
                out << endl;
            }
        }
    };
};
