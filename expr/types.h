#pragma once

#include <boost/variant.hpp>
#include <map>
#include <string>
#include <vector>
#include <functional>
#include "expr/errors.h"
#include "utils.h"

namespace expr {
    namespace types {
        using boost::variant;
        using std::function;
        using std::string;
        using std::vector;

        struct callable;
        typedef variant<
            double,
            string,
            callable,
            vector<double>,
            vector<string>
            > expr_type;

        struct callable {
            typedef function<expr_type(vector<expr_type>)> type;
            type f;

            expr_type operator()() const {
                return this->operator()({});
            }
            expr_type operator()(vector<expr_type> const& v) const {
                return this->f(v);
            }
        };


        auto type_names = std::map<char const*, string>{
            {typeid(double).name(), "double"},
            {typeid(string).name(), "string"},
            {typeid(vector<double>).name(), "vector<double>"},
            {typeid(vector<string>).name(), "vector<string>"},
            {typeid(callable).name(), "callable"},
        };

        template<int ArgCounter, typename Ret>
        Ret call_wrapped_function(function<Ret()> func, vector<expr_type> anyargs) {
            if(anyargs.size() > 0) {
                throw std::runtime_error(
                    supplant(
                        "function call error: argument list too long, more than {%} arguments passed",
                        ArgCounter
                    )
                );
            }
            return func();
        }

        template<int ArgCounter, typename Ret, typename Arg0, typename... Args>
        Ret call_wrapped_function(function<Ret(Arg0, Args...)> func, vector<expr_type> anyargs) {
            if(anyargs.size() == 0) {
                throw std::runtime_error(
                    supplant(
                        "function call error: argument list too short, parameter {%} not found (expected type: %)",
                        ArgCounter, type_names[typeid(Arg0).name()]
                    )
                );
            }
            Arg0 arg0;
            try {
                arg0 = boost::get<Arg0>(anyargs[0]);
            }
            catch(boost::bad_get& e) {
                throw mismatch_types(
                    supplant(
                        "function call error: cannot cast parameter {%} to %",
                        ArgCounter, type_names[typeid(Arg0).name()]
                    )
                );
            }
            anyargs.erase(anyargs.begin());
            function<Ret(Args... args)> lambda = (
                [=](Args... args) -> Ret {
                    return func(arg0, args...);
                }
            );
            return call_wrapped_function<ArgCounter+1, Ret, Args...>(lambda, anyargs);
        }

        template<typename Ret, typename... Args>
        callable wrap_function(function<Ret(Args...)> func) {
            return callable{(
                [=](vector<expr_type> anyargs) -> expr_type {
                    Ret result = call_wrapped_function<0, Ret, Args...>(func, anyargs);
                    return expr_type(result);
                }
            )};
        }

        template<typename Ret, typename... Args>
        callable wrap_function(Ret (func)(Args...)) {
            return wrap_function(function<Ret(Args...)>(func));
        }
    };
};
