#pragma once
#define BOOST_RESULT_OF_USE_DECLTYPE
#define BOOST_SPIRIT_USE_PHOENIX_V3

#include <string>
#include <vector>
#include <boost/spirit/include/qi.hpp>
#include <boost/spirit/include/phoenix.hpp>
#include "expr/ast.h"

namespace boost { namespace spirit { namespace traits
{
    namespace ast = expr::ast;

    template <>
    struct transform_attribute<ast::ValueNode<double> const*, double, qi::domain> {
        using TNode = ast::ValueNode<double> const*;
        using type = double;

        static type pre(TNode& d) {
            return type{};
        }
        static void post(TNode& val, type const& attr) {
            val = new ast::ValueNode<double>(attr);
        }
        static void fail(TNode&) {}
    };

    using std::string;
    using std::vector;

    template <>
    struct transform_attribute<ast::ValueNode<string> const*, vector<char>, qi::domain> {
        using TNode = ast::ValueNode<string> const*;
        using type = vector<char>;

        static type pre(TNode& d) {
            return type{};
        }
        static void post(TNode& val, type const& attr) {
            val = new ast::ValueNode<string>(string(attr.data(), attr.size()));
        }
        static void fail(TNode&) {}
    };

    template <>
    struct transform_attribute<ast::SymbolNode const*, std::string, qi::domain> {
        using TNode = ast::SymbolNode const*;
        using type = std::string;

        static type pre(TNode& d) {
            return type{};
        }
        static void post(TNode& val, type const& attr) {
            val = new ast::SymbolNode(attr);
        }
        static void fail(TNode&) {}
    };
}}}

namespace expr {
    namespace parser {
        namespace ast = expr::ast;
        namespace qi = boost::spirit::qi;
        namespace fusion = boost::fusion;
        namespace phx = boost::phoenix;

        template<typename Iterator, typename Skipper=qi::space_type>
        struct ExprGrammar : qi::grammar<Iterator, ast::Node const*(), Skipper> {
            qi::rule<Iterator, ast::Node const*(), Skipper> expression;
            qi::rule<Iterator, ast::Node const*(), Skipper> term;
            qi::rule<Iterator, ast::Node const*(), Skipper> factor;
            qi::rule<Iterator, ast::Node const*(), Skipper> operand;

            qi::rule<Iterator, ast::FunctionCallNode*(), Skipper> function_call;
            qi::rule<Iterator, ast::SymbolNode const*()> symbol;

            qi::rule<Iterator, ast::ValueNode<double> const*()> number_value;
            qi::rule<Iterator, ast::Node const*(), qi::locals<char>> string_value;
            qi::rule<Iterator, ast::StringifyNode const*()> interpolation;
            qi::rule<Iterator, std::string()> identifier;
            qi::symbols<char const, char const> escape_chars;

            ExprGrammar() : ExprGrammar::base_type(expression) {
                {
                    auto handler = [this](
                        ast::Node const* first,
                        std::vector<fusion::vector2<char, ast::Node const*>> const& values) -> ast::Node const* {
                        ast::Node const* output = first;
                        for(auto& e : values) {
                            output = this->binary_operation(
                                fusion::at_c<0>(e), output, fusion::at_c<1>(e));
                        }
                        return output;
                    };
                    expression = (
                        term
                        >> *(
                            (qi::char_('+') >> term)
                            |(qi::char_('-') >> term)
                        )
                    )[qi::_val = phx::bind(handler, qi::_1, qi::_2)];

                    term = (
                        factor
                        >> *(
                            (qi::char_('*') >> factor)
                            |(qi::char_('/') >> factor)
                        )
                    )[qi::_val = phx::bind(handler, qi::_1, qi::_2)];

                    factor %= (
                        operand
                        | '(' >> expression >> ')'
                    );
                }

                {
                    using operand_signature = boost::variant<
                        ast::FunctionCallNode const*,
                        ast::SymbolNode const*,
                        ast::Node const*,
                        ast::ValueNode<double> const*>;
                    auto handler = [](operand_signature const& value) -> ast::Node const* {
                        ast::Node const* output;
                        if(auto p = boost::get<ast::FunctionCallNode const*>(&value)) {
                            output = *p;
                        }
                        else if(auto p = boost::get<ast::SymbolNode const*>(&value)) {
                            output = *p;
                        }
                        else if(auto p = boost::get<ast::ValueNode<double> const*>(&value)) {
                            output = *p;
                        }
                        else if(auto p = boost::get<ast::Node const*>(&value)) {
                            output = *p;
                        }
                        return output;
                    };

                    operand = (
                        function_call
                        | symbol
                        | number_value
                        | string_value
                    )[qi::_val = phx::bind(handler, qi::_1)];
                }

                function_call = (identifier >> '(' >> -(operand % ',') > ')')[
                    qi::_val = phx::bind([](std::string const& name, boost::optional<std::vector<ast::Node const*>> const& params) -> ast::FunctionCallNode* {
                        ast::FunctionCallNode* output;
                        if(params) {
                            output = new ast::FunctionCallNode(name, *params);
                        }
                        else {
                            output = new ast::FunctionCallNode(name);
                        }
                        return output;
                    }, qi::_1, qi::_2)
                ];

                symbol = identifier[
                    qi::_val = phx::bind([](std::string const& name) {
                            return new ast::SymbolNode(name);
                    }, qi::_1)
                ];

                {
                    using boost::proto::deep_copy;

                    using qi::_a;
                    using qi::_1;
                    using qi::_r1;

                    using attr_signature = std::vector<boost::variant<expr::ast::StringifyNode const*, char>>;
                    auto handler = [this](attr_signature const& elements) -> expr::ast::Node const* {
                        if(elements.size() == 0) {
                            return new expr::ast::ValueNode<std::string>("");
                        }

                        std::vector<expr::ast::Node const*> reassembled;
                        std::string _acc;

                        for(auto& el : elements) {
                            if(auto n = boost::get<expr::ast::StringifyNode const*>(&el)) {
                                if(_acc.size()) {
                                    reassembled.push_back(new expr::ast::ValueNode<std::string>(_acc));
                                    _acc = "";
                                }
                                reassembled.push_back(*n);
                            }
                            else if(auto c = boost::get<char>(&el)) {
                                _acc += *c;
                            }
                        }
                        if(_acc.size()) {
                            reassembled.push_back(new expr::ast::ValueNode<std::string>(_acc));
                        }

                        auto it = reassembled.begin();
                        auto output = *it++;
                        while(it != reassembled.end()) {
                            output = this->binary_operation('+', output, *it++);
                        }
                        return output;
                    };
                    string_value =
                        qi::omit[qi::char_("'\"")[_a = _1]]
                        >> (
                            *(
                                interpolation
                                | qi::lexeme[(escape_chars | (qi::char_ - qi::char_(_a)))]
                            )
                        )[qi::_val = phx::bind(handler, _1)]
                        >> qi::lit(_a);
                }

                interpolation =
                    qi::lit('#')
                    >> '{'
                    >> qi::skip(Skipper())[
                        expression[qi::_val = phx::new_<ast::StringifyNode>(qi::_1)]]
                    >> *Skipper()
                    >>'}';

                number_value = qi::attr_cast(qi::double_);

                identifier = (+qi::alpha >> *qi::alnum)[qi::_val = phx::bind(
                    [](std::vector<char> const & x) -> std::string {
                        return std::string(x.data(), x.size());
                    }, qi::_1)
                ];

                escape_chars.add
                    ("\\a", '\a')
                    ("\\b", '\b')
                    ("\\f", '\f')
                    ("\\n", '\n')
                    ("\\r", '\r')
                    ("\\t", '\t')
                    ("\\v", '\v')
                    ("\\\\", '\\')
                    ("\\\'", '\'')
                    ("\\\"", '\"');

                this->expression.name("expression");
                this->term.name("expression term");
                this->factor.name("expression factor");
                this->operand.name("operand");
                this->function_call.name("function_call");
                this->symbol.name("symbol");
                this->interpolation.name("interpolation");
                this->string_value.name("string");
                this->number_value.name("number");
                this->identifier.name("identifier");

                //qi::debug(this->expression);
                //qi::debug(this->term);
                //qi::debug(this->factor);
                //qi::debug(this->operand);
                //qi::debug(this->function_call);
                //qi::debug(this->symbol);
                //qi::debug(this->string_value);
                //qi::debug(this->interpolation);
                //qi::debug(this->number_value);
                //qi::debug(this->identifier);
            }

            ast::Node const* binary_operation(char op, ast::Node const* first, ast::Node const* second) {
                ast::Node const* output;
                switch(op) {
                    case '+':
                        output = new ast::PlusOperator(first, second);
                        break;
                    case '-':
                        output = new ast::MinusOperator(first, second);
                        break;
                    case '*':
                        output = new ast::MultipliesOperator(first, second);
                        break;
                    case '/':
                        output = new ast::DividesOperator(first, second);
                        break;
                }
                return output;
            }
        };

        ast::Node const* parse(std::string const& src) {
            ExprGrammar<std::string::const_iterator> g;
            auto start = src.begin();
            auto end = src.end();
            ast::Node const* a = nullptr;
            using namespace std;
            //cout << "* start parsing" << endl;
            auto r = qi::phrase_parse(start, end, g, qi::space, a);
            //cout << "* finished --> " << r << endl;
            //cout << a << endl;
            return a;
        }
    };
};
