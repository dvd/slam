#pragma once

#include <string>
#include <vector>
#include "expr/context.h"
#include "expr/types.h"
#include "utils.h"

namespace expr {
    namespace ast {
        using std::string;
        using std::vector;
        using expr::context::Context;
        using expr::types::expr_type;

        struct Node {
            virtual expr_type evaluate(Context const&) const = 0;
        };

        template<typename T>
        struct ValueNode : Node {
            T const value;

            ValueNode(T const& v) : value(v) {};
            virtual expr_type evaluate(Context const& ctx) const {
                return this->value;
            };
        };

        struct SymbolNode : Node {
            string const name;

            SymbolNode(string const& v) : name(v) {};

            virtual expr_type evaluate(Context const& ctx) const {
                return ctx.get(this->name);
            }
        };

        struct FunctionCallNode: Node {
            string const name;
            vector<Node const*> params;

            FunctionCallNode(string const& v, vector<Node const*> const& p) : name(v), params(p) {};
            FunctionCallNode(string const& v) : FunctionCallNode{v, vector<Node const*>()} {};

            virtual expr_type evaluate(Context const& ctx) const {
                using expr::types::callable;

                vector<expr_type> values;
                for(auto const& p : this->params) {
                    values.push_back(p->evaluate(ctx));
                }
                auto f = boost::get<callable>(ctx.get(this->name));
                return f(values);
            }
        };

        struct StringifyNode : Node {
            Node const* child;

            StringifyNode(Node const* c) : child(c) {};

            virtual expr_type evaluate(Context const& ctx) const {
                auto result = this->child->evaluate(ctx);
                struct to_string : boost::static_visitor<string> {
                    string operator()(string const& v) const {
                        return v;
                    }
                    string operator()(double const& v) const {
                        return supplant("%", v);
                    }
                    string operator()(vector<string> const& v) const {
                        return supplant("{vector of strings size=%}", v.size());
                    }
                    string operator()(vector<double> const& v) const {
                        return supplant("{vector of numbers size=%}", v.size());
                    }
                    string operator()(expr::types::callable const& v) const {
                        return supplant("{callable}");
                    }
                };
                return boost::apply_visitor(to_string(), result);
            }
        };

        struct BinaryOperatorVisitor : boost::static_visitor<> {
            template<typename T, typename U>
            void operator()(T&, U&) const {
                throw mismatch_types(supplant(
                    "BinaryOperator cannot be applied to different types: %, %",
                    expr::types::type_names[typeid(T).name()],
                    expr::types::type_names[typeid(U).name()]));
            };

            template<typename T>
            void operator()(T&, T&) const {
                throw mismatch_types(supplant(
                    "BinaryOperator not specialized for type:%",
                    expr::types::type_names[typeid(T).name()]));
            }
        };

        struct BinaryOperator : Node {
            static int const max_priority{1};
            int const priority;
            Node const* first;
            Node const* second;

            BinaryOperator(Node const* f, Node const* s, int p=0) : priority{p}, first(f), second(s) {};

            virtual expr_type evaluate(Context const& ctx) const {
                auto left = this->first->evaluate(ctx);
                auto right = this->second->evaluate(ctx);
                this->apply_on_left(left, right);
                return left;
            };

            protected:
                virtual void apply_on_left(expr_type& left, expr_type& right) const = 0;
        };

        struct PlusOperatorVisitor : BinaryOperatorVisitor {
            using BinaryOperatorVisitor::operator();

            void operator()(double& first, double& second) const {
                first += second;
            }

            void operator()(string& first, string& second) const {
                first += second;
            }
        };

        struct MinusOperatorVisitor : BinaryOperatorVisitor {
            using BinaryOperatorVisitor::operator();

            void operator()(double& first, double& second) const {
                first -= second;
            }
        };

        struct MultipliesOperatorVisitor : BinaryOperatorVisitor {
            using BinaryOperatorVisitor::operator();

            void operator()(double& first, double& second) const {
                first *= second;
            }
        };

        struct DividesOperatorVisitor : BinaryOperatorVisitor {
            using BinaryOperatorVisitor::operator();

            void operator()(double& first, double& second) const {
                first /= second;
            }
        };

        struct PlusOperator : BinaryOperator {
            using BinaryOperator::BinaryOperator;

            protected:
                void apply_on_left(expr_type& left, expr_type& right) const {
                    boost::apply_visitor(PlusOperatorVisitor(), left, right);
                }
        };

        struct MinusOperator : BinaryOperator {
            using BinaryOperator::BinaryOperator;

            protected:
                void apply_on_left(expr_type& left, expr_type& right) const {
                    boost::apply_visitor(MinusOperatorVisitor(), left, right);
                }
        };

        struct MultipliesOperator : BinaryOperator {
            MultipliesOperator(Node const* f, Node const* s) : BinaryOperator(f, s, 1) {};

            protected:
                void apply_on_left(expr_type& left, expr_type& right) const {
                    boost::apply_visitor(MultipliesOperatorVisitor(), left, right);
                }
        };

        struct DividesOperator : BinaryOperator {
            DividesOperator(Node const* f, Node const* s) : BinaryOperator(f, s, 1) {};

            protected:
                void apply_on_left(expr_type& left, expr_type& right) const {
                    boost::apply_visitor(DividesOperatorVisitor(), left, right);
                }
        };

    };
};
