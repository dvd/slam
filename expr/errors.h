#pragma once
#include <stdexcept>

class mismatch_types : public std::runtime_error {
    using std::runtime_error::runtime_error;
};
